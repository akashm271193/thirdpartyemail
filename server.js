var express = require("express");
var app = express();
var port = process.env.PORT || 3000;
var configReader = require('./apis/utilities/configReaderUtility');
configReader.getConfig('./config/config.json');
var db = require('./db');
var emailModel = require('./apis/models/emailModel');
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

var routes = require('./routes');
routes(app);

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);
module.exports = app;
