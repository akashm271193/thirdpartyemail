// db.js
var mongoose = require('mongoose');
var configReader = require('./apis/utilities/configReaderUtility');
mongoose.Promise = global.Promise;
mongoose.connect(configReader.Config.database.connUrl);

