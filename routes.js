module.exports = function(application){
   var emailController = require('./apis/controllers/emailController');
   
   application.route('/send-email').post(emailController.sendEmail);
   application.route('/bounced-email').post(emailController.addEmailToBlackList);	
};
