var configReader = require('../utilities/configReaderUtility');
var emailModel = require('../models/emailModel');

class emailServices{
	
	/**
	  * Constructor Function to instatiate emailServices
          **/

	constructor(){
		this.service = configReader.Config.application.activeEmailService;
		this.setServiceProviderObject();
	}

	
	/**
	  * Function to send emails
    	  * Takes params (body,res) and sends  the email as per the respective provider set in the object
	  **/

	sendEmail(body,res){
		var message = emailModel.getMessageBodyForEmailService(this.service,body);
		this.serviceProvider[this.sendFunction](message, function(err, data) {
                	if (err) {
                        	res.status(400).json({error:err.message}); // an error 
                	}
                	else{
                        	res.status(200).json({message:"Successfully sent email"}) // Successful response 
                	}
        	});
	}

	/**
	  * Function to set the Service Provider based on the values passed from config.json
	  **/

	setServiceProviderObject(){
		if(this.service == "aws"){
			var AWS = require('aws-sdk');
			var config = configReader.Config.emailServices[this.service];
			var creds = new AWS.Credentials(config.accessKeyId, config.secretAccessKey);
			AWS.config.update({region:config.region,credentials: creds});
			this.serviceProvider  = new AWS.SES();
			this.sendFunction = "sendEmail";
		}
		else if(this.service == "sendGrid"){
			this.serviceProvider = require('@sendgrid/mail');
			this.serviceProvider.setApiKey(configReader.Config.emailServices[this.service].apiKey);
			this.sendFunction = "send";
		}
	}
}



module.exports = emailServices;
