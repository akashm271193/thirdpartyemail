var mongoose = require('mongoose');


// Create a new schema as required in the DB
var emailSchema = new mongoose.Schema({
    email:{
    	type: String,
        required:[true,'Please provide an email to blacklist'],
	validate:{
		isAsync:true,
		/**
		  * Validation function to verify the email
		  **/
		validator:function(v,cb){
			var emailRegEx = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i;
			cb(emailRegEx.test(v));
		},
		message : "Invalid email Address"	
	}
    },
    createdDate:{
    	type:Date,
	default:Date.now
    }
});

module.exports = mongoose.model('EmailBlacklist',emailSchema);

/**
* Function to generate Message Body as per the type of Email Service
* Takes params service- Name of the service and body - Message Data  and returns the populated object
*/

module.exports.getMessageBodyForEmailService = function(service,body){
	var params = {
		"aws":{
			Destination: {
                        	BccAddresses: [],
                        	CcAddresses: [],
                        	ToAddresses: [body.to]
                	},
                	Message: {
                        	Body: {
                                	Html: {
                                        	Charset: "UTF-8",
                                        	Data: body.body_html
                                	},
                                	Text: {
                                        	Charset: "UTF-8",
                                        	Data: body.body_text
                                	}
                        	},
                        	Subject: {
                                	Charset: "UTF-8",
                                	Data: body.subject
                        	}
                	},
                	Source: body.from
		},
		"sendGrid":{
			to: body.to,
                	from:body.from,
                	subject: body.subject,
                	text: body.body_text,
                	html: body.body_html
		}
	};

	return params[service];
};
