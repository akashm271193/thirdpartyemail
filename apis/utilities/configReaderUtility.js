var fs = require('fs');

/**
* Function to read the config.json file and load it in modules.export.Config variable.
*/

module.exports.getConfig = function(filePath){
	var rawData = fs.readFileSync(filePath);
	var config  = JSON.parse(rawData);
	module.exports.Config = config;
}  
