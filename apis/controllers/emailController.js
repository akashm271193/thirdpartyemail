var mongoose = require("mongoose");
emailBlacklist = mongoose.model('EmailBlacklist');
var emailServices = require("../services/emailServices");

/**
*Function to validate an Email address. Takes in an email address and returns true if valid else false
**/
var validateEmail = function(email){
	var emailRegEx = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i;
        return emailRegEx.test(email);
}

/**
* Function to add emails to blaclist
* Takes params (req,res) and adds the email in the mongo DB
*/
module.exports.addEmailToBlackList = function(req,res){	
        if(!validateEmail(req.body.email_address)){
		res.status(400).json({"error":"Invalid Email Id"});
		return;
	};
	var query = {email:req.body.email_address};
	var options = {upsert:true, setDefaultsOnInsert:true,runValidators:true};
	emailBlacklist.findOneAndUpdate(query,query,options,function(err,doc){
		if(err){
			res.status(400).json({"error":err.errors.email.message});
		}
		else{
			res.status(200).json({"message":"Email successfully black listed !"});
		}
	});
};


/**
* Function to send Email
* Takes params (req,res) , creates and instance of EmailService and sends the email
**/
module.exports.sendEmail = function(req,res){
	if(!validateEmail(req.body.to)){
		res.status(400).json({"error":"Invalid To Email Id"});
                return;
	}
	if(!validateEmail(req.body.from)){
		res.status(400).json({"error":"Invalid From Email Id"});
                return;
	}
	if(req.body.subject == undefined || req.body.subject == null){
		res.status(400).json({"error":"Please provide a Subject"});
                return;

	}
	if(req.body.body_html == undefined || req.body.body_html == null){
                res.status(400).json({"error":"Please provide a Body html"});
                return;

        }
	if(req.body.body_text == undefined || req.body.body_text == null){
                res.status(400).json({"error":"Please provide a Body Text"});
                return;

        }
	var query = {email : {$in : [req.body.to, req.body.from]}};
	emailBlacklist.find(query,function(err,data){
		if(err){
			res.status(500).json({error:"Oops! We are facing some issues. Please try again after sometime."});
		
		}
		else{
			if(data.length != 0){
				res.status(400).json({error:"Sorry, we are unable to process this request. Please contact us for further information !"});
			}
			else{
				 var service = new emailServices();
       				 service.sendEmail(req.body,res);
			}
		}

	});
};
