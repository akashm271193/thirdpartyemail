A simple application in Node.js using MongoDB database which integrates two services AWS SES and SendGrid Email service:
1.)  The server will listen to PORT if set as and environment variable or 3000(default)for two endpoints - /send-email  & /bounced-email.
2.) /send-email takes in a POST Request with parameters - from, to, subject, body_text, body_html.
     - On receiving the request , it checks if any of the 'to' or 'for'  email ids is in the database or not. If present, it rejects the request to send the mail
     - If the email is not present, it will call the AWS-SES/SendGrid API based on the json configuration and handle the responses appropriately.
3.) /bounced-email will take in a POST Request with a single argument in the url - email_address .
     - It will again check the DB for the existence, and appropriately add it to the DB.
4.) Config file consists of a json structure of multiple objects like application, database and emailServices
